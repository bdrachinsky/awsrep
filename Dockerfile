FROM mhart/alpine-node:latest


RUN echo $DK_CONFIG > env_file

MAINTAINER Kyle Polich

RUN mkdir -p /usr/src/app

# Install app dependencies
COPY package.json /usr/src/app/
COPY . /usr/src/app

WORKDIR /usr/src/app

RUN npm install


EXPOSE 3000

ENTRYPOINT ["npm", "run", "start"]

